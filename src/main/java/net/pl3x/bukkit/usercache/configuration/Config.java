package net.pl3x.bukkit.usercache.configuration;

import net.pl3x.bukkit.usercache.Main;

@SuppressWarnings("unused")
public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    AUTOSAVE_INTERVAL(60);

    private final Main plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public static void reload() {
        Main.getPlugin(Main.class).reloadConfig();
    }
}
