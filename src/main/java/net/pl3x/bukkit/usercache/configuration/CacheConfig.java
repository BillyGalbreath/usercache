package net.pl3x.bukkit.usercache.configuration;

import net.pl3x.bukkit.usercache.Logger;
import net.pl3x.bukkit.usercache.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class CacheConfig extends YamlConfiguration {
    public static final String NAME = ".name";
    public static final String DISPLAY_NAME = ".display-name";
    public static final String LAST_IP = ".last-ip";
    public static final String LAST_LOCATION = ".last-location";
    public static final String LAST_CHAT = ".last-chat";
    public static final String LAST_COMMAND = ".last-command";

    private static CacheConfig config;

    public static CacheConfig getConfig() {
        if (config == null) {
            config = new CacheConfig();
        }
        return config;
    }

    private final File file;
    private final Object saveLock = new Object();
    private boolean cacheDirty = false;

    private CacheConfig() {
        super();
        file = new File(Main.getPlugin(Main.class).getDataFolder(), "cache.yml");
        reload();
    }

    public void reload() {
        synchronized (saveLock) {
            try {
                if (!file.exists()) {
                    if (!file.createNewFile()) {
                        Logger.debug("Problem creating cache file! Load failed!");
                        return;
                    }
                }
                load(file);
                Logger.debug("Cache file loaded.");
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        synchronized (saveLock) {
            try {
                save(file);
                cacheDirty = false;
                Logger.debug("Cache changes were saved to disk.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isDirty() {
        return cacheDirty;
    }

    public void setDirty() {
        cacheDirty = true;
    }

    public void setLocation(String key, Location location) {
        set(key + ".world", location.getWorld().getName());
        set(key + ".x", location.getX());
        set(key + ".y", location.getY());
        set(key + ".z", location.getZ());
        set(key + ".yaw", location.getYaw());
        set(key + ".pitch", location.getPitch());
    }

    public Location getLocation(String key) {
        String worldName = getString(key + ".world", "");
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            return null;
        }
        double x = getDouble(key + ".x");
        double y = getDouble(key + ".y");
        double z = getDouble(key + ".z");
        float yaw = (float) getDouble(key + ".yaw", 0);
        float pitch = (float) getDouble(key + ".pitch", 0);
        return new Location(world, x, y, z, yaw, pitch);
    }
}
