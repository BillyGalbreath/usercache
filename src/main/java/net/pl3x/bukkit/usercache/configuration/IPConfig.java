package net.pl3x.bukkit.usercache.configuration;

import net.pl3x.bukkit.usercache.Logger;
import net.pl3x.bukkit.usercache.Main;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class IPConfig extends YamlConfiguration {
    private static IPConfig config;

    public static IPConfig getConfig() {
        if (config == null) {
            config = new IPConfig();
        }
        return config;
    }

    private final File file;
    private final Object saveLock = new Object();
    private boolean dirty = false;

    private IPConfig() {
        super();
        file = new File(Main.getPlugin(Main.class).getDataFolder(), "ip.yml");
        reload();
    }

    public void reload() {
        synchronized (saveLock) {
            try {
                if (!file.exists()) {
                    if (!file.createNewFile()) {
                        Logger.debug("Problem creating IP file! Load failed!");
                        return;
                    }
                }
                load(file);
                Logger.debug("IP file loaded.");
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        synchronized (saveLock) {
            try {
                save(file);
                dirty = false;
                Logger.debug("IP changes were saved to disk.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty() {
        dirty = true;
    }
}
