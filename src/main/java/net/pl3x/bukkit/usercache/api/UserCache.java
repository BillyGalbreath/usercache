package net.pl3x.bukkit.usercache.api;

import net.pl3x.bukkit.usercache.configuration.CacheConfig;
import net.pl3x.bukkit.usercache.configuration.IPConfig;

import java.util.UUID;

@SuppressWarnings("unused")
public class UserCache {
    /**
     * Get a CachedPlayer by name
     *
     * @param name Name of player
     * @return CachedPlayer
     */
    public static CachedPlayer getCachedPlayer(String name) {
        if (name == null) {
            return null;
        }
        CacheConfig cache = CacheConfig.getConfig();
        for (String uuid : cache.getKeys(false)) {
            String realName = cache.getString(uuid + CacheConfig.NAME);
            if (realName.equalsIgnoreCase(name)) {
                return new CachedPlayer(UUID.fromString(uuid));
            }
        }
        return null;
    }

    /**
     * Get a CachedPlayer by name or IP address
     *
     * @param nameOrIP Name or IP address of player
     * @return CachedPlayer
     */
    public static CachedPlayer getCachedPlayerByNameOrIP(String nameOrIP) {
        if (nameOrIP == null) {
            return null;
        }
        IPConfig ipConfig = IPConfig.getConfig();
        String uuidFromIP = ipConfig.getString(nameOrIP);
        if (uuidFromIP != null && !uuidFromIP.isEmpty()) {
            return new CachedPlayer(UUID.fromString(uuidFromIP));
        }
        CacheConfig cache = CacheConfig.getConfig();
        for (String uuid : cache.getKeys(false)) {
            String name = cache.getString(uuid + CacheConfig.NAME);
            String ip = cache.getString(uuid + CacheConfig.LAST_IP);
            if (name.equalsIgnoreCase(nameOrIP) || ip.equals(nameOrIP)) {
                return new CachedPlayer(UUID.fromString(uuid));
            }
        }
        return null;
    }

    /**
     * Get a CachedPlayer by uuid
     *
     * @param uuid UUID of player
     * @return CachedPlayer
     */
    public static CachedPlayer getCachedPlayer(UUID uuid) {
        if (isInCache(uuid)) {
            return new CachedPlayer(uuid);
        }
        return null;
    }

    /**
     * Test if player name or IP address exists in cache
     *
     * @param nameOrIP Name or IP address of player
     * @return True if is in cache
     */
    public static boolean isInCache(String nameOrIP) {
        if (nameOrIP == null) {
            return false;
        }
        CacheConfig cache = CacheConfig.getConfig();
        for (String uuid : cache.getKeys(false)) {
            String name = cache.getString(uuid + CacheConfig.NAME);
            String ip = cache.getString(uuid + CacheConfig.LAST_IP);
            if (name.equalsIgnoreCase(nameOrIP) || ip.equals(nameOrIP)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test if player uuid exists in cache
     *
     * @param uuid UUID of player
     * @return True if is in cache
     */
    public static boolean isInCache(UUID uuid) {
        return CacheConfig.getConfig().isSet(uuid.toString());
    }
}
