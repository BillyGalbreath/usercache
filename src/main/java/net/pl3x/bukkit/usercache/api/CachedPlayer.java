package net.pl3x.bukkit.usercache.api;

import net.pl3x.bukkit.usercache.configuration.CacheConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

@SuppressWarnings({"WeakerAccess", "unused"})
public class CachedPlayer {
    private final UUID uuid;

    protected CachedPlayer(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Get the UUID for this player
     *
     * @return UUID
     */
    public UUID getUniqueId() {
        return uuid;
    }

    /**
     * Get the name for this player
     *
     * @return Name
     */
    public String getName() {
        return CacheConfig.getConfig().getString(uuid + CacheConfig.NAME);
    }

    /**
     * Get the last known display name for this player
     * <p>
     * If no display name was ever set, their real name is used
     *
     * @return Last known display name
     */
    public String getDisplayName() {
        String displayName = CacheConfig.getConfig().getString(uuid + CacheConfig.DISPLAY_NAME);
        if (displayName == null) {
            displayName = CacheConfig.getConfig().getString(uuid + CacheConfig.NAME);
        }
        return displayName;
    }

    /**
     * Get the IP address for this player
     *
     * @return IP Address
     */
    public String getLastKnownIP() {
        return CacheConfig.getConfig().getString(uuid + CacheConfig.LAST_IP);
    }

    /**
     * Get the location this player last logged out at
     *
     * @return Location
     */
    public Location getLastKnownLocation() {
        return CacheConfig.getConfig().getLocation(uuid + CacheConfig.LAST_LOCATION);
    }

    /**
     * Get Bukkit Player object if player is currently online
     *
     * @return Player
     */
    public Player getPlayer() {
        return getOfflinePlayer().getPlayer();
    }

    /**
     * Get Bukkit OfflinePlayer object
     *
     * @return OfflinePlayer
     */
    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(getUniqueId());
    }
}
