package net.pl3x.bukkit.usercache;

import net.pl3x.bukkit.usercache.configuration.CacheConfig;
import net.pl3x.bukkit.usercache.configuration.Config;
import net.pl3x.bukkit.usercache.listener.CacheListener;
import net.pl3x.bukkit.usercache.task.AutoSave;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private AutoSave autoSaveTask;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Bukkit.getPluginManager().registerEvents(new CacheListener(), this);

        autoSaveTask = new AutoSave();
        autoSaveTask.runTaskTimer(this, 20L, Config.AUTOSAVE_INTERVAL.getInt() * 20L);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        CacheConfig cache = CacheConfig.getConfig();
        for (Player player : Bukkit.getOnlinePlayers()) {
            cache.set(player.getUniqueId() + CacheConfig.LAST_LOCATION, player.getLocation());
            cache.setDirty();
        }

        if (autoSaveTask != null) {
            autoSaveTask.run();
            autoSaveTask.cancel();
            autoSaveTask = null;
        }

        Logger.info(getName() + " disabled!");
    }

}
