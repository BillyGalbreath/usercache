package net.pl3x.bukkit.usercache.listener;

import net.pl3x.bukkit.usercache.Logger;
import net.pl3x.bukkit.usercache.api.UserCache;
import net.pl3x.bukkit.usercache.configuration.CacheConfig;
import net.pl3x.bukkit.usercache.configuration.IPConfig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class CacheListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        String ip = player.getAddress().getAddress().getHostAddress();

        IPConfig ipConfig = IPConfig.getConfig();
        ipConfig.set(ip, uuid.toString());
        ipConfig.setDirty();

        boolean updating = UserCache.isInCache(uuid);

        CacheConfig cache = CacheConfig.getConfig();
        cache.set(uuid + CacheConfig.NAME, player.getName());
        cache.set(uuid + CacheConfig.DISPLAY_NAME, player.getDisplayName());
        cache.set(uuid + CacheConfig.LAST_IP, ip);
        cache.setDirty();

        Logger.debug((updating ? "Updated" : "Added") + " player cache entry.");
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        CacheConfig cache = CacheConfig.getConfig();
        cache.setLocation(uuid + CacheConfig.LAST_LOCATION, player.getLocation());
        cache.set(uuid + CacheConfig.DISPLAY_NAME, player.getDisplayName());
        cache.setDirty();

        Logger.debug("Saved player's last location");
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        CacheConfig cache = CacheConfig.getConfig();
        cache.set(uuid + CacheConfig.LAST_CHAT, event.getMessage());
        cache.set(uuid + CacheConfig.DISPLAY_NAME, player.getDisplayName());
        cache.setDirty();

        Logger.debug("Saved player's last chat message.");
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        CacheConfig cache = CacheConfig.getConfig();
        cache.set(uuid + CacheConfig.LAST_COMMAND, event.getMessage());
        cache.set(uuid + CacheConfig.DISPLAY_NAME, player.getDisplayName());
        cache.setDirty();

        Logger.debug("Saved player's last command.");
    }
}
