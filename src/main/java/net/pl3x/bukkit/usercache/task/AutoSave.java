package net.pl3x.bukkit.usercache.task;

import net.pl3x.bukkit.usercache.configuration.CacheConfig;
import net.pl3x.bukkit.usercache.configuration.IPConfig;
import org.bukkit.scheduler.BukkitRunnable;

public class AutoSave extends BukkitRunnable {
    @Override
    public void run() {
        CacheConfig cache = CacheConfig.getConfig();
        IPConfig ip = IPConfig.getConfig();

        if (cache.isDirty()) {
            cache.save();
        }
        if (ip.isDirty()) {
            ip.save();
        }
    }
}
