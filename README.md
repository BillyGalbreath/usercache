UserCache is a dependency plugin that other plugins hook into for cached offline player data. This plugin was created due to Mojang's built in cache going stale after about 30 days and deleting users from record. As an added bonus more features are now cached, like last known name, uuid, ip, location, chat message, and command (more to come on demand).

This plugin by itself does nothing other than cache player data. It is only useful to other plugins that hook into it. These other plugins will explicitly specify if they require this plugin.

There are no commands or permissions. Just simply place the jar into your plugin directory to install it.

https://www.spigotmc.org/resources/usercache.20537/